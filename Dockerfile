FROM ubuntu:14.04

MAINTAINER Christophe Maldivi

RUN apt-get update \
    && apt-get install -y tsocks traceroute curl host iputils-tracepath \
    && apt-get install -y python-wxgtk2.8 \
    && apt-get install -y python python-pip python-dev libffi-dev libssl-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#RUN pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U

ADD requirements.txt .
#RUN pip install --upgrade pip
RUN pip install -U setuptools
RUN pip install -U -r requirements.txt

RUN mkdir /robot
VOLUME /robot
WORKDIR /robot
CMD ["ride.py"]